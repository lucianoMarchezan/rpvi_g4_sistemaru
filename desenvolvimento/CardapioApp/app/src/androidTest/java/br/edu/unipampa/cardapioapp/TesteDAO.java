package br.edu.unipampa.cardapioapp;

import android.content.Context;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import br.edu.unipampa.cardapioapp.dao.DAORefeicao;
import br.edu.unipampa.cardapioapp.model.Refeicao;

import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by joao_ on 15/10/2016.
 */
@RunWith(AndroidJUnit4.class)
public class TesteDAO {

    Context context;

    @Before
    public void setup() {
        context = InstrumentationRegistry.getTargetContext();


    }

    @Test
    public void select_positivo() throws Exception {

        DAORefeicao dao = new DAORefeicao(context);



        assertEquals(4,dao.pesquisarTodos().size());

    }



}
