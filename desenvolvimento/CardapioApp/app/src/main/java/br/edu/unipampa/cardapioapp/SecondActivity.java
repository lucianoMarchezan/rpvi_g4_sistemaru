package br.edu.unipampa.cardapioapp;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import br.edu.unipampa.cardapioapp.dao.DAORefeicao;
import br.edu.unipampa.cardapioapp.model.Refeicao;

public class SecondActivity extends AppCompatActivity {


    private Button gostei;
    private Button nGostei;
    private TextView data;
    private TextView refeicaoC;
    private TextView pratoPrincipal;
    private TextView acompanhamento;
    private TextView guarnicao;
    private TextView saladas;
    private TextView sobremesa;
    private TextView suco;
    private TextView opcVegetariana;

    DAORefeicao daoRefeicao = new DAORefeicao(this);


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);


            data = (TextView) findViewById(R.id.cardapio_data);
            refeicaoC = (TextView) findViewById(R.id.cardapio_refeicao);
            pratoPrincipal = (TextView) findViewById(R.id.text_PratoPrincipal);
            acompanhamento = (TextView) findViewById(R.id.text_Acompanhamento);
            guarnicao = (TextView) findViewById(R.id.text_Guarnicao);
            saladas = (TextView) findViewById(R.id.text_Salada);
            sobremesa = (TextView) findViewById(R.id.text_Sobremesa);
            suco = (TextView) findViewById(R.id.text_Suco);
            gostei = (Button) findViewById(R.id.button);
            nGostei = (Button) findViewById(R.id.button2);



        Bundle bundle = getIntent().getBundleExtra("main");
        final Refeicao refeicao = (Refeicao)bundle.getSerializable("refeicao");



        data.setText(refeicao.getData());
        refeicaoC.setText(refeicao.getNomeRefeicao());
        pratoPrincipal.setText(refeicao.getPratoPrincipal());
        acompanhamento.setText(refeicao.getAcompanhamentos());
        guarnicao.setText(refeicao.getGuarnicao());
        saladas.setText(refeicao.getSaladas());
        sobremesa.setText(refeicao.getSobremesa());
        suco.setText(refeicao.getSuco());


        gostei.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    //VARIAVEIS DE AUXILIO PARA EXIBIR MENSAGEM DE SUCESSO
                    Context contexto = getApplicationContext();
                    String texto = "AVALIADO COM SUCESSO";
                    int duracao = Toast.LENGTH_SHORT;


                    refeicao.setAvaliacao("Gostei");
                    daoRefeicao.update(refeicao);


                    //EXIBE MENSAGEM DE SUCESSO
                    Toast toast = Toast.makeText(contexto, texto, duracao);
                    toast.show();
                    finish();
                    Intent voltar2 = new Intent(SecondActivity.this, MainActivity.class);
                    startActivity(voltar2);

                }

            });


            nGostei.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Context contexto = getApplicationContext();
                    String texto = "AVALIADO COM SUCESSO";
                    int duracao = Toast.LENGTH_SHORT;

                    refeicao.setAvaliacao("Não Gostei");
                    daoRefeicao.update(refeicao);



                    Toast toast = Toast.makeText(contexto, texto, duracao);
                    toast.show();
                    finish();

                    Intent voltar = new Intent(SecondActivity.this, MainActivity.class);
                    startActivity(voltar);

                }
            });



    }
}

