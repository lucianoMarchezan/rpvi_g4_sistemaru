package br.edu.unipampa.cardapioapp.controller;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import br.edu.unipampa.cardapioapp.R;
import br.edu.unipampa.cardapioapp.model.Refeicao;

/**
 * Created by Leonardo Anschau on 08/10/2016.
 */

public class RefeicaoController extends AppCompatActivity {


    private Button gostei;
    private Button nGostei;
    private TextView acao;
    private String resultado;
    private boolean avaliacao;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        gostei = (Button) findViewById(R.id.button);
        nGostei = (Button) findViewById(R.id.button2);
        acao = (TextView) findViewById(R.id.textView);


        gostei.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Context contexto = getApplicationContext();
                String texto = "AVALIADO COM SUCESSO";
                int duracao = Toast.LENGTH_SHORT;

                avaliacao = true;
                resultado = "gostei";
                acao.setText(resultado);

                Toast toast = Toast.makeText(contexto, texto, duracao);
                toast.show();
            }

        });

        nGostei.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Context contexto = getApplicationContext();
                String texto = "AVALIADO COM SUCESSO";
                int duracao = Toast.LENGTH_SHORT;

                avaliacao = false;
                resultado = "não gostei";
                acao.setText(resultado);

                Toast toast = Toast.makeText(contexto, texto, duracao);
                toast.show();
            }
        });


    }
}
