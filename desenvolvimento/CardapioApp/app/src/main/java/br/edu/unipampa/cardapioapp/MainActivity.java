package br.edu.unipampa.cardapioapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.AutoText;
import android.util.Log;
import android.view.View;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.AdapterViewAnimator;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Objects;
import java.util.concurrent.locks.ReentrantLock;

import br.edu.unipampa.cardapioapp.dao.DAORefeicao;
import br.edu.unipampa.cardapioapp.model.Refeicao;
import br.edu.unipampa.cardapioapp.view.ListViewAdapter;

public class MainActivity extends AppCompatActivity implements Serializable {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Log.i("Teste", "Teste!");
        setContentView(R.layout.activity_main);


        DAORefeicao dao = new DAORefeicao(this);


        //EXECUTA AQUI PARA PREENCHER O BANCO
        final Refeicao refeicao = new Refeicao("Janta", "Lasanha", "Farofa",
                "Massa", "Alface", "Pudim", "Limão",
                "20/05", "Brócolis");
        dao.salvar(refeicao);
        //dao.update(refeicao);


        final ArrayList<Refeicao> refeicoes;
        refeicoes = dao.pesquisarTodos();
        final ListViewAdapter adapter = new ListViewAdapter(this, R.layout.activity_list_view_adapter, refeicoes);

        final ListView list = (ListView) findViewById(R.id.listView);
        list.setAdapter(adapter);
        /*list.setOnItemClickListener(new OnItemClickListener());
        //listViewItems.setOnItemClickListener(new OnItemClickListenerListViewItem());*/

        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Refeicao refeicao = (Refeicao) parent.getItemAtPosition(position);


                Bundle bundle = new Bundle();
                bundle.putLong("id", refeicao.getId());
                bundle.putString("data", refeicao.getData());
                bundle.putString("tipo", refeicao.getNomeRefeicao());
                bundle.putString("prato principal", refeicao.getPratoPrincipal());
                bundle.putString("acompanhamento", refeicao.getAcompanhamentos());
                bundle.putString("avaliacao", refeicao.getAvaliacao());
                bundle.putString("saladas", refeicao.getSaladas());
                bundle.putString("vegetariana", refeicao.getOpcaoVeg());
                bundle.putString("suco", refeicao.getSuco());
                bundle.putString("sobremesa", refeicao.getSobremesa());
                bundle.putString("guarnicao", refeicao.getGuarnicao());

                bundle.putSerializable("refeicao", refeicao);




                Intent intent = new Intent(getBaseContext(), SecondActivity.class);

                intent.putExtra("main", bundle);

                if(refeicao.getAvaliacao().equals("Avaliar")){
                    startActivity(intent);
                }else{
                    Toast.makeText(MainActivity.this, "Refeição Avaliada!", Toast.LENGTH_SHORT).show();
                }



            }
        });

    }
}
